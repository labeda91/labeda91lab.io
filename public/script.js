$(document).ready(function() {
    function animateProgressBar() {
    
      let windowHeight = $(window).height();
      
      let windowBottom = $(window).scrollTop() + windowHeight;
     
      $('.progress-bar').each(function() {
       
        let progressTop = $(this).offset().top;

        if (windowBottom > progressTop) {
         
          let targetValue = $(this).data('target-value');

          $(this).find('.progress-fill').css('width', targetValue + '%');
        }
      });
    }

    $(window).on('load scroll', animateProgressBar);
  });